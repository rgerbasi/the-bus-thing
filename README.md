# Ionic Dev Search - Bus Thing
Sample project app for the Ionic team

### Technology
* [Polymer 3.0 Preview](https://github.com/Polymer/polymer/tree/3.0-preview)
* [Webpack](https://webpack.js.org/)
* [LeafletJS](http://leafletjs.com/)

### Prerequisites
* [Node 8.4.0+](https://nodejs.org/en/download/current/)
* [Chrome 61+](https://www.google.com/chrome/browser/desktop/index.html)

### Installing
Clone this Repo and install dependencies
```
git clone git@bitbucket.org:rgerbasi/the-bus-thing.git
cd the-bus-thing
npm i
```

### Development
Start Development using the Webpack Dev Server
```
npm run start
```

### Build

Builds a production version (minified, transpiled and stripped)
```
npm run build:prod
```

Builds a development version (sourcemaps)
```
npm run buld:dev
```

### Testing

Runs tests (test/**.spec.js)
```
npm run test
```