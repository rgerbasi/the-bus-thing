const path = require('path'),
    BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
    UglifyJSPlugin = require('uglifyjs-webpack-plugin'),
    GoogleFontsPlugin = require('google-fonts-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function (env = {}) {
    const param = (prop, fallback) => env[prop] !== undefined ? env[prop] : fallback,
        BUILD = param('build', false),
        LINT = param('lint', true),
        TRANSPILE = param('transpile', false),
        MINIFY = param('minify', false),
        STRIP = param('strip', false),
        SOURCEMAPS = param('sourcemaps', true);

    return {
        context: path.resolve(__dirname, 'src'),
        entry: {
            main: './bootstrap.js'
        },
        devtool: SOURCEMAPS ? 'inline-source-map' : false,
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: '[name].bundle.js'
        },
        devServer: {
            inline: true,
            historyApiFallback: true,
            host: '0.0.0.0',
            disableHostCheck: true
        },
        module: {
            rules: [
                {
                    test: /\.html$/, use: 'html-loader'
                },
                {
                    test: /\.css$/,
                    use: [
                        {loader: 'style-loader'},
                        {loader: 'css-loader'}
                    ]
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192
                            }
                        }
                    ]
                },
                STRIP ? {
                    test: /\.js$/,
                    enforce: 'pre',
                    exclude: /(node_modules|bower_components|\.spec\.js)/,
                    use: [{
                        loader: 'webpack-strip-blocks',
                        options: {
                            blocks: ['debug'],
                            start: '/*',
                            end: '*/'
                        }
                    }]
                } : false,
                TRANSPILE ? {
                    test: /\.js$/,
                    exclude: /node-modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }
                } : false,
                LINT ? {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: 'eslint-loader'
                } : false
            ].filter((rule) => !!rule)
        },
        plugins: [
            BUILD ? new CleanWebpackPlugin(['dist']) : false,
            new GoogleFontsPlugin({
                fonts: [
                    {
                        family: 'Roboto',
                        variants: ['300', '400', '700']
                    }
                ],
                local: false,
                formats: ['woff']
            }),
            new HtmlWebpackPlugin({
                shim: TRANSPILE,
                minify: MINIFY ? {
                    collapseWhitespace: true,
                    preserveLineBreaks: false,
                    minifyCSS: true,
                    minifyJS: true
                } : false,
                template: path.resolve(__dirname, 'index.ejs')
            }),
            new CopyWebpackPlugin([
                {
                    from: path.resolve(__dirname, 'assets'),
                    to: 'assets'
                },
                {
                    from: path.resolve(__dirname, 'node_modules', '@webcomponents', 'webcomponentsjs', 'webcomponents-*'),
                    flatten: true
                },
                TRANSPILE ? {
                    from: path.resolve(__dirname, 'node_modules', '@webcomponents', 'webcomponentsjs', 'custom-elements-es5-adapter.js')
                } : false
            ].filter((copy) => !!copy)),
            MINIFY ? new UglifyJSPlugin() : false,
            BUILD ? false : new BundleAnalyzerPlugin({openAnalyzer: false})
        ].filter((plugin) => !!plugin)
    };
};
