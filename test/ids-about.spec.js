import {expect} from 'chai';
import '../src/ids-about';

describe('Creation Test', function () {
    beforeEach(() => {
        document.body.insertAdjacentHTML(
            'afterbegin',
            `
              <div id="fixture">
                <ids-about></ids-about>
              </div>
            `);
    });

    afterEach(() => {
        document.body.removeChild(document.getElementById('fixture'));
    });

    it('Should be true', function () {
        const tag = document.querySelector('ids-about');
        expect(tag.clientWidth).to.be.above(0);
    });
});
