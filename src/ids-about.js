import {Element as PolymerElement} from '@polymer/polymer/polymer-element';
import '@polymer/iron-flex-layout/iron-flex-layout';

/**
 * Simple element to display an About page
 */
export class IdsAbout extends PolymerElement {
    static get template () {
        return `
            <!--suppress CssUnresolvedCustomProperty, CssUnresolvedCustomPropertySet -->
            <style>
                :host {
                    @apply(--layout-vertical);
                    @apply(--layout-center);
                    @apply(--layout-center-justified);
                    color: white;
                }
                
                h2 {
                    font-size: 12vw;
                }
                
                div {
                    font-weight: 100;
                }
            </style>
           <h2>SF TRANSIT</h2>
           <div>A programming test for the folks at Ionic. 🚀</div>
        `;
    }

    constructor () {
        super();
    }
}

customElements.define('ids-about', IdsAbout);
