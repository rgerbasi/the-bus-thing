import {Element as PolymerElement} from '@polymer/polymer/polymer-element';
import '@polymer/app-route/app-location';
import '@polymer/app-route/app-route';
import '@polymer/iron-flex-layout/iron-flex-layout';
import '@polymer/iron-pages/iron-pages';
import '@polymer/iron-icons/iron-icons';
import '@polymer/app-layout/app-drawer/app-drawer';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout';
import '@polymer/app-layout/app-header/app-header';
import '@polymer/app-layout/app-toolbar/app-toolbar';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-icon-button/paper-icon-button';
// No Dynamic/Lazy imports yet for Polymer 3.0 :(
import './ids-map';
import './ids-about';

/**
 * <ids-transit> Is a element is a top level application element for
 * the SF Transit app. It provides connectivity for toolbar icons and selection of current
 * page.
 *
 * It also provides application navigation with an app drawer
 */
export class IdsTransit extends PolymerElement {
    static get template () {
        return `
            <!--suppress CssUnresolvedCustomProperty, CssUnresolvedCustomPropertySet -->
            <style>
                :host {
                    font-family: 'Roboto', 'Noto', sans-serif;
                    color: var(--paper-grey-900);
                }
                
                app-drawer-layout {
                    background-color: var(--paper-grey-900);
                }
                
                app-header {
                    background-color: var(--paper-orange-300);
                    font-weight: bold;
                }
                
                app-drawer paper-button {
                    --paper-button: {
                        @apply --layout-start-justified;
                        width: 100%;
                        margin: 0;
                    }
                }
                
                app-header-layout {
                    width: 100%;
                    height: 100%;
                    @apply(--layout-vertical);
                    @apply(--layout-flex);
                }
                
                iron-pages {
                    @apply(--layout-flex);
                    @apply(--layout-horizontal);
                }
                
                iron-pages > * {
                    @apply(--layout-flex);
                    @apply(--layout-vertical);
                }
                
                [hidden] {
                    display: none;
                }
                
                app-toolbar paper-icon-button[icon-parent] {
                  transition: opacity 500ms ease-out;
                }
                
                app-toolbar paper-icon-button[icon-parent][inactive] {
                  opacity: 0;
                  pointer-events: none;
                }
            </style>
            
            <app-location route="{{route}}" use-hash-as-path></app-location>
            <app-route
                route="[[route]]"
                pattern=":page"
                data="{{routeData}}"></app-route>
                
            <app-drawer-layout force-narrow fullbleed>
              <app-drawer id="drawer" slot="drawer">
                <iron-selector>
                    <template is="dom-repeat" items="{{navigationItems}}" selected="[[page]]">
                        <paper-button name$="[[item.id]]" on-tap="onNavigationClick">[[item.label]]</paper-button>
                    </template>
            </template>
                </iron-selector>
              </app-drawer>
              <app-header-layout>
                <app-header slot="header">
                  <app-toolbar id="toolbar">
                    <paper-icon-button icon="menu" drawer-toggle></paper-icon-button>
                    <div main-title>SF Transit</div>
                  </app-toolbar>
                </app-header>
                <iron-pages id="content" selected="[[page]]" attr-for-selected="name" selected-attribute="visible" fallback-selection="map">
                    <ids-map name="map" route="6"></ids-map>
                    <ids-about name="about"></ids-about>
                </iron-pages>
              </app-header-layout>  
            </app-drawer-layout>
`;
    }

    // noinspection JSUnusedGlobalSymbols
    static get observers () {
        return [
            'routePageChanged(routeData.page)'
        ];
    }

    // noinspection JSUnusedGlobalSymbols
    static get properties () {
        return {
            /**
             * Current page to be showing in the application
             * @protected
             */
            page: {
                type: String,
                reflectToAttribute: true
            },
            /**
             * Used by the app-router and app-location to share data
             * @protected
             */
            routeData: Object,
            /**
             * Collection of navigation items to display in the app drawer
             * ID should map to a name of a tag inside iron-pages
             */
            navigationItems: {
                type: Array,
                value: function () {
                    return [
                        {
                            label: 'Map',
                            id: 'map'
                        },
                        {
                            label: 'About',
                            id: 'about'
                        }
                    ];
                }
            }
        };
    }

    /**
     * Event handler used to catch main content changes.
     * This method then provides an interface in which subviews can control the
     * icons that are displayed in the toolbar
     * @param {Element} value Currently active element from icon-pages
     */
    onContentChange ({detail: {value}}) {
        if (value) {
            // clean up previous icons
            this.$.toolbar.querySelectorAll('paper-icon-button[icon-parent]')
                .forEach((icon) => {
                    if (icon.getAttribute('icon-parent') !== value.tagName) {
                        icon.setAttribute('inactive', true);
                    }
                });
            if (value && value.getToolbarIcons) {
                const icons = value.getToolbarIcons();
                if (icons && icons.length) {
                    icons.forEach((icon) => {
                        icon.removeAttribute('inactive');

                        if (icon.parentNode !== this.$.toolbar) {
                            icon.setAttribute('icon-parent', value.tagName);
                            this.$.toolbar.appendChild(icon);
                        }
                    });
                }
            }
        }
    }

    /**
     * Event handler used when a navigation item from the app drawer is clicked
     * @param {Event} e
     */
    onNavigationClick (e) {
        this.set('route.path', e.target.getAttribute('name'));
    }

    ready () {
        super.ready();
        this.$.content.addEventListener('selected-item-changed', this.onContentChange.bind(this));
    }

    /**
     * Observer method called anytime the page location hash is changed
     * @param {String} page
     */
    routePageChanged (page) {
        if (!page) {
            this.set('route.path', 'map');
            return;
        }

        if (this.page !== page) {
            this.page = page;
            this.$.drawer && (this.$.drawer.opened = false);
        }
    }
}

customElements.define('ids-transit', IdsTransit);
