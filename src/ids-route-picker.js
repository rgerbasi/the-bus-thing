import {Element as PolymerElement} from '@polymer/polymer/polymer-element';
import {mixinBehaviors} from '@polymer/polymer/lib/legacy/class';
import {PaperDialogBehavior} from '@polymer/paper-dialog-behavior/paper-dialog-behavior';
import '@polymer/paper-radio-button/paper-radio-button';
import '@polymer/paper-radio-group/paper-radio-group';

import {RouteListDevData} from './ids-dev-data';

/**
 * <ids-route-picker> is a dialog that contains all the routes for an area
 * it will create a radio button group and allow selection of routes
 * @customElement
 * @polymer
 */
export class IdsRoutePicker extends mixinBehaviors([PaperDialogBehavior], PolymerElement) {
    static get template () {
        return `
         <!--suppress CssUnresolvedCustomProperty, CssUnresolvedCustomPropertySet, CssUnusedSymbol -->
         <style>
            :host {
                @apply(--layout-vertical);
                background: white;
                color: black;
                padding: 24px;
                box-shadow: rgba(0, 0, 0, 0.24) -2px 5px 12px 0, rgba(0, 0, 0, 0.12) 0 0 12px 0;
                max-height: 80%;
            }
            
            h2 {
                margin-top: 0;
            }
            
            paper-radio-group {
                @apply(--layout-vertical);
                overflow: scroll;
                padding: 4px;
                
            }
        </style>
        <h2>SF Muni - Routes</h2>
        <paper-radio-group id="routePickerRadioGroup">
        </paper-radio-group>
        `;
    }

    // noinspection JSUnusedGlobalSymbols
    static get properties () {
        return {
            /**
             * Currently selected routeTag
             */
            route: {
                type: String,
                observer: 'onRouteChange'
            },

            /**
             * @see {PaperDialogBehavior}
             */
            modal: {
                type: Boolean,
                value: false
            },

            /**
             * * @see {PaperDialogBehavior}
             */
            withBackdrop: {
                type: Boolean,
                value: true
            }
        };
    }

    constructor () {
        super();

        this.feedURL = 'http://webservices.nextbus.com/service/publicJSONFeed';

        /* debug:start */
        this.useDevData = false;
        /* debug:end */
    }

    // noinspection JSMethodCanBeStatic
    /**
     * Event Handler called when the dialog is opened
     * @param {Event} e
     */
    // This is a hacky fix for Polymer's z-index/overlay "situation"
    onOverlayOpened (e) {
        if (e.target.withBackdrop) {
            e.target.parentNode.appendChild(e.target.backdropElement);
        }
    }

    /**
     * Event handler called when the route is changed via the tag attribute
     * @param {String} value
     */
    onRouteChange (value) {
        this.$.routePickerRadioGroup.set('selected', value);
    }

    /**
     * Event handler called when the radio group selection changes the route
     * @param {Object} detail event details for the selection
     */
    onRouteSelected ({detail}) {
        this.dispatchEvent(new CustomEvent('route-change', {detail}));
        this.close();
    }

    // noinspection JSUnusedGlobalSymbols
    ready () {
        super.ready();
        this.$.routePickerRadioGroup.addEventListener('selected-changed', this.onRouteSelected.bind(this));
        this.addEventListener('iron-overlay-opened', this.onOverlayOpened.bind(this));
        this.refreshRouteData();
    }

    /**
     * Updates the list of routes
     * @return {Promise} resolves with data from the server containing a list of routes
     */
    refreshRouteData () {
        return this.useDevData ? this._refreshRouteCachedData() : this._refreshRouteData();
    }

    /**
     * Creates radio buttons for each route in the data object
     * @param {Object} data routeList data from the nextbus server
     */
    updateRouteData (data) {
        const routes = data && data.route;
        if (Array.isArray(routes)) {
            const radioGroup = this.$.routePickerRadioGroup;

            radioGroup.innerHTML = '';

            routes.forEach((route) => {
                const radioButton = document.createElement('paper-radio-button');
                radioButton.name = route.tag;
                radioButton.innerText = route.title;
                radioGroup.appendChild(radioButton);
            });
        }
    }

    _refreshRouteData () {
        const url = `${this.feedURL}?command=routeList&a=sf-muni`;
        return fetch(url).then((r) => {
            return r.json();
        }).then((data) => {
            return this.updateRouteData(data);
        });
    }

    _refreshRouteCachedData () {
        return this.updateRouteData(RouteListDevData);
    }
}

customElements.define('ids-route-picker', IdsRoutePicker);
