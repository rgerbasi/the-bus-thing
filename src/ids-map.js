import {Element as PolymerElement} from '@polymer/polymer/polymer-element';
import '@polymer/iron-flex-layout/iron-flex-layout';
import {beforeNextRender} from '@polymer/polymer/lib/utils/render-status';
import 'leaflet/src/control/index';
import 'leaflet/src/core/index';
import 'leaflet/src/dom/index';
import 'leaflet/src/geometry/index';
import 'leaflet/src/geo/index';
import {
    tileLayer,
    marker,
    icon,
    layerGroup,
    circleMarker,
    polyline
} from 'leaflet/src/layer/index';
import {Map as LeafletMap} from 'leaflet/src/map/index';
import leafletStyles from '!!css-loader!leaflet/dist/leaflet.css';
import 'gsap/TweenLite';
import 'gsap/EasePack';

import {RouteDevData, VehicleDevData} from './ids-dev-data';
import './ids-route-picker';
import './ids-timer';

/**
 * <ids-map> is used to wrap a Leaflet Map instance and provide
 * an interface to filter a route and draw vehicles, routes and stops
 * @customElement
 * @polymer
 */
export class IdsMap extends PolymerElement {
    // noinspection JSUnusedGlobalSymbols
    static get properties () {
        return {
            /**
             * Provided from ion-pages, this is used to determine if the
             * map is active on screen
             * @private
             */
            visible: {
                type: Boolean,
                observer: '_onVisibleChange'
            },
            /**
             * Determines which route is active on the map
             */
            route: {
                type: String,
                reflectToAttribute: true,
                value: '6',
                observer: '_onRouteChange'
            },
            /**
             * Determines if the path for the currently active around should be drawn
             */
            showRoute: {
                type: Boolean,
                value: true,
                observer: '_onShowRouteChange'
            }
        };
    }

    static get template () {
        return `
            <!--suppress CssUnresolvedCustomProperty, CssUnresolvedCustomPropertySet, CssUnusedSymbol -->
            <style>
              ${leafletStyles}
            </style>
            <style>  
                #map {
                    @apply(--layout);
                    @apply(--layout-flex-auto);
                }
                
                /* Move the map down to 50 from 400 (default for leaflet)
                    Polymer overlays start at 101
                */
                .leaflet-pane {
                    z-index: 50;
                }
                
                .leaflet-top {
                    z-index: 75;
                
                }
                
                ids-timer {
                    position: absolute;
                    bottom: 20px;
                    right: 10px;
                    width: 25px;
                    height: 25px;
                    background-color: var(--paper-amber-100);
                    z-index: 100;
                    opacity: .8;
                    
                    --ids-timer-fill: {
                        background-color: var(--paper-amber-600);;
                    }
                }
            </style>
            <div id="map"></div>
            <ids-route-picker id="routePicker" route="[[route]]" tabindex="-1">
            </ids-route-picker>
            <ids-timer id="timer" interval="[[getTimerInterval()]]"></ids-timer>
        `;
    }

    /**
     * Transforms vehicle data for use by the map.
     * @param {Object} vehicle vehicle data from nextbus
     * @return {Object} parse vehicle data for ids-map
     */
    static transformVehicleData (vehicle) {
        vehicle = Object.assign({}, vehicle);
        vehicle.lat = parseFloat(vehicle.lat);
        vehicle.lon = parseFloat(vehicle.lon);
        vehicle.latlng = {lat: vehicle.lat, lng: vehicle.lon};
        vehicle.speedKmHr = parseInt(vehicle.speedKmHr);
        vehicle.heading = parseInt(vehicle.heading);
        vehicle.secsSinceReport = parseInt(vehicle.secsSinceReport);
        delete vehicle.dirTag;
        delete vehicle.id;

        return vehicle;
    }

    constructor () {
        super();
        this.busIcon = icon({
            iconUrl: 'assets/images/bus.svg',
            iconSize: [52, 64],
            iconAnchor: [26, 32],
            shadowUrl: 'assets/images/bus-shadow.png',
            shadowSize: [30, 20],
            shadowAnchor: [20, 5]
        });

        /* debug:start */
        this.useDevData = false;
        /* debug:end */
        this.feedURL = 'http://webservices.nextbus.com/service/publicJSONFeed';
        this.lastRequestTime = 0;
        this.vehicles = new Map();
        this.routeData = new Map();
    }

    /**
     * Removes stops and paths from the map
     */
    clearRoute () {
        this.routePathsLayer && this.routePathsLayer.remove();
        this.routeStopsLayer && this.routeStopsLayer.remove();
    }

    connectedCallback () {
        super.connectedCallback();

        // We need to wait until this element is rendered
        // so leaflet can measure the space for the map
        beforeNextRender(this, () => {
            this.map.invalidateSize();
        });
    }

    /**
     * Creator method for a vehicle marker.
     * @param {Object} data vehicle data object containing the lat/lng for the vehicle
     * @return {Marker} Leaflet marker instance
     */
    createVehicleMarker (data) {
        return marker(data.latlng, {
            icon: this.busIcon
        });
    }

    /**
     * Draws paths and stops for the currently active route onto the map
     * If Path and route data is not yet loaded this method will first load that data
     * @param {Boolean} recursive internal variable used to determine if this method was
     * called recursively.
     */
    drawRoute (recursive = false) {
        const route = this.route;

        this.clearRoute();
        if (route && this.showRoute) {
            const routeData = this.routeData.get(route);

            if (routeData) {
                const stops = routeData.stop.map((stop) => {
                    return circleMarker([stop.lat, stop.lon], {
                        stroke: false,
                        fillColor: routeData.color,
                        fillOpacity: 1,
                        radius: 6
                    });
                });

                this.routePathsLayer = polyline(routeData.path, {
                    color: routeData.color,
                    opacity: .5
                }).addTo(this.map);
                this.routeStopsLayer = layerGroup(stops).addTo(this.map);
            } else if (!recursive) {
                this.refreshRoutePathData(true).then(this.drawRoute.bind(this, true));
            } else {
                // problems... how did this madness happen :(
                console.error('unable to find route data');
            }
        }
    }

    /**
     * Determines which elements should be added/shown in the toolbar
     * @return {Array} Collection of element to be added to the toolbar
     */
    // TODO: This method could be moved into some kind of abstract base for all views
    getToolbarIcons () {
        let filterButton = this.filterButton,
            pathVisibilityButton = this.pathVisibilityButton;

        if (!filterButton) {
            filterButton = this.filterButton = document.createElement('paper-icon-button');
            filterButton.setAttribute('icon', 'filter-list');
            filterButton.addEventListener('click', this.showRoutePicker.bind(this));
        }

        if (!pathVisibilityButton) {
            pathVisibilityButton = this.pathVisibilityButton = document.createElement('paper-icon-button');
            pathVisibilityButton.setAttribute('icon', this.showRoute ? 'visibility-off' : 'visibility');
            pathVisibilityButton.addEventListener('click', () => {
                this.showRoute = !this.showRoute;
            });
        }

        return [pathVisibilityButton, filterButton];
    }

    getTimerInterval () {
        return this.useDevData ? 5000 : 20000;
    }

    /**
     * Event handler for route change
     * @param {Event} e
     */
    onRouteChangeFromPicker (e) {
        this.route = e.detail.value;
    }

    /**
     * Event handler for refresh timer triggering
     */
    onTimerTrigger () {
        this.refreshVehicleData();
    }

    ready () {
        super.ready();
        this.$.timer.addEventListener('trigger', this.onTimerTrigger.bind(this));
        this.$.routePicker.addEventListener('route-change', this.onRouteChangeFromPicker.bind(this));

        this.map = new LeafletMap(this.$.map, {zoomControl: false});
        this.map.setView([37.735, -122.445], 12);

        tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.map);

        if (this.refreshDataOnReady) {
            this.refreshRoutePathData().then(this.refreshVehicleData.bind(this));
            this.refreshDataOnReady = null;
        }
    }

    /**
     * Resolves with Path data for the currently active route. This can come from local
     * cache or the server
     * @param {Boolean} force forces a refresh form the server regardless of cache
     * @return {Promise}
     */
    refreshRoutePathData (force = false) {
        const route = this.route,
            url = `${this.feedURL}?command=routeConfig&a=sf-muni&r=${this.route}`;

        return new Promise((resolve) => {
            if (this.routeData.has(route) && !force) {
                resolve(this.routeData.get(route));
            } else {
                if (this.useDevData) {
                    resolve(RouteDevData);
                } else {
                    fetch(url).then((r) => {
                        return r.json();
                    }).then(resolve);
                }
            }
        }).then((data) => {
            return this.updateRoutePathData(route, data);
        });
    }

    /**
     * Resolves with data on vehicles for the currently active route.
     * @return {Promise}
     */
    refreshVehicleData () {
        // console.log(`updating with ${this.useDevData ? 'cached' : 'real'} data for route ${this.route}`);
        const refresh = this.useDevData ?
            this._refreshVehicleCachedData.bind(this) : this._refreshVehicleData.bind(this);
        return refresh().then(this.updateVehicleData.bind(this));
    }

    /**
     * Opens the Route Picker modal
     */
    showRoutePicker () {
        this.$.routePicker.open();
    }

    /**
     * Helper method to start the vehicle position refresh timer
     */
    startTimer () {
        this.$.timer && this.$.timer.start();
    }

    /**
     * Helper method to stop the vehicle position refresh timer
     */
    stopTimer () {
        this.$.timer && this.$.timer.stop();
    }

    /**
     * Transforms and updates cached data for a route. Cleaning up position
     * and slimming down saved data
     * @param {String} route RouteTag for the route to update
     * @param {Array} stop Collection of stops
     * @param {String} title title for the current route
     * @param {String} color hexcolor for this route
     * @param {Array} path collection of points to plot the line for this route
     * @return {Object}
     */
    updateRoutePathData (route, {route: {stop, title, color, path}}) {
        stop = stop.map((stop) => {
            stop.lat = parseFloat(stop.lat);
            stop.lon = parseFloat(stop.lon);
            return stop;
        });

        path = path.map((pathObj) => {
            return pathObj.point.map((point) => {
                return [parseFloat(point.lat), parseFloat(point.lon)];
            });
        });
        this.routeData.set(route, {stop, title, color: `#${color}`, path});
        return this.routeData.get(route);
    }

    /**
     * Updates local map of vehicles with latest information from the server
     * @param {Object} data vehicle data from the nextbus server
     */
    updateVehicleData (data) {
        let vehicles = (data && data.vehicle) || [];
        this.lastTime = (data && data.lastTime
            && parseInt(data.lastTime.time)) || 0;

        if (!Array.isArray(vehicles)) {
            vehicles = [vehicles];
        }

        vehicles.forEach((vehicle) => {
            const transformedVehicleData = IdsMap.transformVehicleData(vehicle),
                cachedVehicle = this.vehicles.get(vehicle.id);

            if (!cachedVehicle) {
                this.vehicles.set(vehicle.id, transformedVehicleData);
            } else {
                this.vehicles.set(vehicle.id, Object.assign(cachedVehicle, transformedVehicleData));
            }
            this.updateVehicleMarker(vehicle.id);
        });
    }

    /**
     * Updates the position of a vehicle on the map
     * @param {String} id vehicle id
     * @param {Object} [data] transformed vehicle data
     */
    updateVehicleMarker (id, data) {
        data = data ? data : this.vehicles.get(id);

        if (data) {
            let vehicleMarker = data.vehicleMarker;
            if (!vehicleMarker) {
                vehicleMarker = this.createVehicleMarker(data);
                data.vehicleMarker = vehicleMarker.addTo(this.map);
                vehicleMarker.setLatLng(data.latlng);
            } else {
                const latlng = vehicleMarker.animatedLatLng = vehicleMarker.getLatLng();
                // eslint-disable-next-line no-undef
                vehicleMarker.tween = TweenLite.to(latlng, 500 / 1000, {
                    lat: data.lat,
                    lng: data.lon,
                    // eslint-disable-next-line no-undef
                    ease: Elastic.easeOut,
                    onUpdate: this.onTweenUpdate.bind(this),
                    onUpdateParams: [vehicleMarker],
                    onComplete: () => {
                        delete vehicleMarker.tween;
                        delete vehicleMarker.animatedLatLng;
                    }
                });
            }

            // We set this in case the data was passed in as a param.
            this.vehicles.set(id, data);
        }
    }

    // noinspection JSMethodCanBeStatic
    /**
     * Heartbeat method used to animate a marker
     * @param {Marker} vehicleMarker
     */
    onTweenUpdate (vehicleMarker) {
        vehicleMarker.setLatLng(vehicleMarker.animatedLatLng);
    }

    _onRouteChange () {
        this.$.timer && this.$.timer.stop();
        this.vehicles.forEach((vehicle) => {
            const vehicleMarker = vehicle.vehicleMarker;
            if (vehicleMarker) {
                vehicleMarker.remove();
            }
        });
        this.vehicles.clear();

        this.lastRequestTime = 0;

        // Map sure we have a map to place markers into before we get to crazy
        if (this.map) {
            this.refreshVehicleData();
            if (this.showRoute) {
                this.drawRoute();
            }
        } else {
            this.refreshDataOnReady = true;
        }
        this.startTimer();
    }

    _onShowRouteChange (value) {
        const pathVisibilityButton = this.pathVisibilityButton;
        if (value) {
            this.drawRoute();
        } else {
            this.clearRoute();
        }

        pathVisibilityButton && pathVisibilityButton.setAttribute('icon', value ? 'visibility-off' : 'visibility');
    }

    _onVisibleChange (value) {
        if (value) {
            this.map.invalidateSize();
            this.startTimer();
        } else {
            this.stopTimer();
        }
    }

    _refreshVehicleData () {
        const url = `${this.feedURL}?command=vehicleLocations&a=sf-muni&r=${this.route}&t=${this.lastRequestTime}`;
        return fetch(url).then((r) => {
            return r.json();
        }).then((data) => {
            this.lastRequestTime = parseInt((data && data.lastTime && data.lastTime.time) || 0);
            return data;
        });
    }

    _refreshVehicleCachedData () {
        const data = VehicleDevData;

        data.vehicle.map((vehicle) => {
            vehicle.lat = parseFloat(vehicle.lat) + ((Math.random() - .5) / 75);
            vehicle.lon = parseFloat(vehicle.lon) + ((Math.random() - .5) / 75);
        });

        return new Promise((resolve) => resolve(data));
    }
}

customElements.define('ids-map', IdsMap);
