import {Element as PolymerElement} from '@polymer/polymer/polymer-element';
import 'gsap/TweenLite';
import 'gsap/EasePack';
import 'gsap/CSSPlugin';

/**
 * <ids-timer> is a visual timer element. Fires both a tick and interval event to
 * allow for progress and triggering of a timed method
 */
export class IdsTimer extends PolymerElement {
    static get template () {
        return `
            <!--suppress CssUnresolvedCustomProperty, CssUnresolvedCustomPropertySet -->
            <style>
                :host {
                    border-radius: 50%;
                    overflow: hidden;
                }
                
                :host([inactive]) {
                  filter: grayscale(100%);
                }
                
                #fill {
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                
                    @apply --ids-timer-fill;
                }
            </style>
            
            <div id="fill"></div>
        `;
    }

    // noinspection JSUnusedGlobalSymbols
    static get properties () {
        return {
            /**
             * Determines the interval in which progress is dispatched
             */
            tick: {
                type: Number,
                observer: 'onTickChange',
                value: 1000
            },
            /**
             * Determines the interval in which a trigger is dispatched
             */
            interval: {
                type: Number,
                observer: 'onIntervalChange',
                value: 20000
            }
        };
    }

    constructor () {
        super();
        this.stop();

        this.minInterval = 15000;
        this.minTick = 1000;
        this.paused = false;

        this.addEventListener('click', this.onClick.bind(this));
    }

    /**
     * Event handled called when this element is clicked on.
     * Caused the timer to toggle paused state
     */
    onClick () {
        this.togglePause();
    }

    /**
     * Callback from setInterval. This is the heartbeat of the timer
     */
    onTick () {
        if (this.interval && !this.paused) {
            this.tickCount++;
            const progress = Math.min(1, Math.max(0, (this.tickCount * this.tick) / this.interval));
            let height = `${progress * 100}%`;

            // this.dispatchEvent(new CustomEvent('progress', {detail: {progress}}));
            if (progress === 1) {
                height = '0%';
                this.dispatchEvent(new CustomEvent('trigger'));
                this.tickCount = 0;
            }
            // eslint-disable-next-line no-undef
            TweenLite.to(this.$.fill, .25, {css: {height: height}});
        }
    }

    /**
     * Observer for interval value change, restarts the timer with new interval
     * @param {Number} value
     */
    onIntervalChange (value) {
        this.interval = Math.max(this.minInterval, value);
        this.restart();
    }

    /**
     * Observer for tick chance, restarts the timer with new tick
     */
    onTickChange () {
        this.restart();
    }

    /**
     * Restarts the timer
     */
    restart () {
        this.stop();
        this.start();
    }

    /**
     * Starts the timer if it is currently not running
     */
    start () {
        if (this.tickInterval) {
            this.stop();
        }
        this.tickInterval = setInterval(this.onTick.bind(this), Math.max(this.minTick, this.tick));
    }

    /**
     * Stops the timer and clears any information about currently running timer.
     */
    stop () {
        clearInterval(this.tickInterval);
        this.tickInterval = null;
        this.tickCount = 0;
    }

    /**
     * Toggles the paused state of the timer
     */
    togglePause () {
        this.paused = !this.paused;
        this.paused ? this.setAttribute('inactive', true) : this.removeAttribute('inactive');
    }
}

customElements.define('ids-timer', IdsTimer);
